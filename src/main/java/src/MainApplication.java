package src;

import java.io.IOException;

/**
 * Main class of the program
 * @author Dziuba Andrii
 */
public class MainApplication {

    public MainApplication() {
        printHelpInformation();
        loop();
    }

    /**
     * Method which prints information about how this program works to the console
     */
    public void printHelpInformation() {
        System.out.println("Welcome to another wild application!!!");
        System.out.println("I can print out odd and even numbers and their sum from typed interval");
        System.out.println("Or get and print the biggest odd and even numbers from Fibonacci sequence");
        System.out.println("Also, i can calculate for you percentage of these numbers in set");
        System.out.println("Ok. Let\'s go");
    }

    /**
     * Helper method, which farewell with user
     */
    private void quit() {

        System.exit(0);
    }

    /**
     * Main cycle of the program. For first, it prints usage guide to the console. Reads user input.
     *
     */
    public void loop() {
        try (CustomScanner scanner = new CustomScanner()) {
            while(true) {
                System.out.println("What are you want to do?");
                System.out.println("Type two real numbers separated inline to get info about numbers between them");
                System.out.println("Or type single real number to work with Fibonacci sequence");
                System.out.println("--- Type (help) if you're stuck ---");
                System.out.println("--- Type (exit) to quit ---");

                // TODO: Check maximum value of N, which depends on long max value

                int[] typedNumbers = null;

                if (scanner.hasNext()) {
                    String typedString = scanner.nextLine();

                    if ("help".equals(typedString.trim())) {
                        printHelpInformation();
                    }
                    if ("exit".equals(typedString.trim())) {
                        System.out.println("See you later, bye!! =)");
                        break;
                    }

                    String[] splittedString = typedString.split(" ");
                    typedNumbers = new int[splittedString.length];

                    for (int i = 0; i < splittedString.length; i++) {
                        try {
                            typedNumbers[i] = Integer.parseInt(splittedString[i]);
                        } catch (NumberFormatException e) {
                            System.out.println("You've got a mistake. Please write numbers again");
                        }
                    }
                    if (typedNumbers.length > 2) {
                        System.out.println("Typed too many numbers. Ok, I shall operate with two first");
                    }
                    if (typedNumbers.length == 1) {
                        int N = typedNumbers[0];
                        if (N <= 0) {
                            System.out.println("N must be natural number");
                            continue;
                        }
                        long F1 = FibonacciUtils.getBiggestOddFibonacciNumberInSetOfN(N);
                        long F2 = FibonacciUtils.getBiggestEvenFibonacciNumberInSetOfN(N);
                        float oddPercentage = FibonacciUtils.getPercentageOfOddNumbersInFibonacciSequenceToN(N);
                        float evenPercentage = FibonacciUtils.getPercentageOfEvenNumbersInFibonacciSequenceToN(N);
                        System.out.println("Typed number N = " + N);
                        System.out.println("Biggest odd number F1 = " + F1);
                        System.out.println("Biggest even number F2 = " + F2);
                        System.out.println("Odd numbers percentage of set - " + oddPercentage + "%");
                        System.out.println("Even numbers percentage of set - " + evenPercentage + "%");
                        System.out.println();
                    } else {
                        int n1 = typedNumbers[0];
                        int n2 = typedNumbers[1];
                        System.out.println("Odd numbers of interval in ascending order:");
                        IntervalUtils.printAllOddNumbersFromStartToEnd(n1, n2);
                        System.out.println("Even numbers of interval in descending order:");
                        IntervalUtils.printAllEvenNumbersFromEndToStart(n1, n2);
                        System.out.println("Sum of odd numbers from interval:");
                        System.out.println(IntervalUtils.getSumOfOddNumbersInTheInterval(n1, n2));
                        System.out.println("Sum of even numbers from interval:");
                        System.out.println(IntervalUtils.getSumOfEvenNumbersInTheInterval(n1, n2));
                    }
                }
            }
        }catch (IOException e ){}
    }

    public static void main(String[] args) {
        new MainApplication();
    }
}
