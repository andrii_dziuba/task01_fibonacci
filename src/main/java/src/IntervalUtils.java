package src;

/**
 * Utility class for handling intervals
 *
 * @author Dziuba Andrii
 */
public class IntervalUtils {

    private static int _start, _end;

    /**
     * Method prints out all odd numbers from <code>start</code> to <code>end</code>
     *
     * @param start - begin of the interval
     * @param end   - last number of the interval
     */
    public static void printAllOddNumbersFromStartToEnd(int start, int end) {
        prepareNumbers(start, end);
        if (_start != _end) {
            _start = (_start & 1) == 1 ? _start : _start + 1;
            for (int i = _start; i <= _end; i += 2) {
                System.out.print(i + " ");
            }
        } else {
            if ((_start & 1) == 1) {
                System.out.print(_start);
            } else {
                System.out.print("No such numbers that satisfying conditions");
            }
        }
        System.out.println();
    }

    /**
     * Method prints out all even numbers from <code>start</code> to <code>end</code>
     *
     * @param start - begin of the interval
     * @param end   - last number of the interval
     */
    public static void printAllEvenNumbersFromEndToStart(int end, int start) {
        prepareNumbers(start, end);
        if (_start != _end) {
            _end = (_end & 1) == 0 ? _end : _end - 1;
            for (int i = _end; i >= _start; i -= 2) {
                System.out.print(i + " ");
            }
        } else {
            if ((_end & 1) == 0) {
                System.out.print(_end);
            } else {
                System.out.print("No such numbers that satisfying conditions");
            }
        }
        System.out.println();
    }

    /**
     * This method counts the sum of the all odd numbers inside the interval
     *
     * @param start - begin of the interval
     * @param end   - last number of the interval
     */
    public static long getSumOfOddNumbersInTheInterval(int start, int end) {
        prepareNumbers(start, end);
        long sum = 0;
        if (_start != _end) {
            _start = (_start & 1) == 1 ? _start : _start + 1;
            for (int i = _start; i <= _end; i += 2) {
                sum += i;
            }
        } else {
            if ((_start & 1) == 1) {
                sum = _start;
            } else {
                System.out.print("No such numbers that satisfying conditions");
            }
        }
        return sum;
    }

    /**
     * This method counts the sum of the all even numbers inside the interval
     *
     * @param start - begin of the interval
     * @param end   - last number of the interval
     */
    public static long getSumOfEvenNumbersInTheInterval(int start, int end) {
        prepareNumbers(start, end);
        long sum = 0;
        if (_start != _end) {
            _end = (_end & 1) == 0 ? _end : _end - 1;
            for (int i = _end; i >= _start; i -= 2) {
                sum += i;
            }
        } else {
            if ((_end & 1) == 0) {
                sum = _end;
            } else {
                System.out.print("No such numbers that satisfying conditions");
            }
        }
        return sum;
    }

    /**
     * Helper method, which allows to exclude th situation when the <code>start<code/> of interval is bigger than <code>end</code>
     *
     * @param a
     * @param b
     */
    private static void prepareNumbers(int a, int b) {
        if (a > b) {
            _start = b;
            _end = a;
        } else {
            _start = a;
            _end = b;
        }
    }
}
