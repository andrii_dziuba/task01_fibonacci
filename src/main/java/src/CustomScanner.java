package src;

import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * Custom scanner class with realized <code>Closeable</code> interface
 * @author Dziuba Andrii
 */
public class CustomScanner implements Closeable {

    private Scanner scanner = null;

    public CustomScanner() {
        scanner = new Scanner(System.in);
    }

    public boolean hasNext() {
        return scanner.hasNext();
    }

    /**
     * Method overrided from <code>Closeable</code> interface
     * @throws IOException - default exception int method signature
     * @throws RuntimeException - just cause
     */
    public void close() throws IOException {
        scanner.close();
    }

    /**
     * nextLine method. Really??
     * @return scanner.nextLine()
     */
    public String nextLine() {
        return scanner.nextLine();
    }
}
