package src;

/**
 * Utility class for handling Fibonacci sequence
 *
 * @author Dziuba Andrii
 */
public class FibonacciUtils {

    private static long[] fibonacciArray = null;

    /**
     * This method builds an array of <code>long</code> and fills in numbers from Fibonacci sequence
     *
     * @param N
     */
    private static void calculateFibonacciSequence(int N) {
        fibonacciArray = new long[N];
        fibonacciArray[0] = 1;
        if (N > 1) {
            fibonacciArray[1] = 1;
            for (int i = 2; i < N; i++) {
                fibonacciArray[i] = fibonacciArray[i - 1] + fibonacciArray[i - 2];
            }
        }
    }

    /**
     * Get the biggest odd number of Fibonacci set
     *
     * @param N
     * @return The biggest odd number
     */
    public static long getBiggestOddFibonacciNumberInSetOfN(int N) {
        if (fibonacciArray == null || fibonacciArray.length != N) {
            calculateFibonacciSequence(N);
        }
        long F1 = 0;
        for (int i = fibonacciArray.length - 1; i >= 0; i--) {
            long c = fibonacciArray[i];
            if ((c & 1) == 1) {
                F1 = c;
                break;
            }
        }

        return F1;
    }

    /**
     * Get the biggest even number of Fibonacci set
     *
     * @param N
     * @return The biggest even number
     */
    public static long getBiggestEvenFibonacciNumberInSetOfN(int N) {
        if (fibonacciArray == null || fibonacciArray.length != N) {
            calculateFibonacciSequence(N);
        }
        long F2 = 0;
        if (N < 3) {
            return F2;
        }
        for (int i = fibonacciArray.length - 1; i >= 0; i--) {
            long c = fibonacciArray[i];
            if ((c & 1) == 0) {
                F2 = c;
                break;
            }
        }

        return F2;
    }

    /**
     * Calculates the percent of odd numbers of all set
     *
     * @param N
     * @return part of odd numbers in set in percents
     */
    public static float getPercentageOfOddNumbersInFibonacciSequenceToN(int N) {
        if (fibonacciArray == null || fibonacciArray.length != N) {
            calculateFibonacciSequence(N);
        }
        int oddNumersCounter = 0;
        for (long i : fibonacciArray) {
            if ((i & 1) == 1) {
                oddNumersCounter++;
            }
        }

        float oddPercentage = (float) oddNumersCounter / fibonacciArray.length;
        return oddPercentage;
    }

    /**
     * Calculates the percent of even numbers of all set.
     * Method simply calls <code>100f - getPercentageOfOddNumbersInFibonacciSequenceToN(N)</code>
     *
     * @param N
     * @return part of even numbers in set in percents
     */
    public static float getPercentageOfEvenNumbersInFibonacciSequenceToN(int N) {
        float oddPercentage = getPercentageOfOddNumbersInFibonacciSequenceToN(N);
        float evenPercentage = 100.0f - oddPercentage;
        return evenPercentage;
    }

}
